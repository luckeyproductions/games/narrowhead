/* Narrowhead
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "materials.h"

Material* Materials::Cloth(const Shade& shade)
{
    if (!cloth_.Contains(shade))
    {
        SharedPtr<Material> cloth{ RES(Material, "Materials/Cloth.xml")->Clone() };

        Color diff{ shade.ToColor() };
        diff.FromHSV(diff.Hue(), diff.SaturationHSV() * .9f, diff.Value());
        Color spec{};
        spec.FromHSV(diff.Hue() + .5f,
                     diff.SaturationHSV() * .75f,
                     Lerp(diff.Value(), .25f, .75f),
                     2.3f);

        cloth->SetShaderParameter("MatDiffColor", diff);
        cloth->SetShaderParameter("MatSpecColor", spec);
        cloth_.Insert({ shade, cloth });
    }

    return cloth_[shade];
}

Material* Materials::Paint(const Shade& shade)
{
    if (!paint_.Contains(shade))
    {
        SharedPtr<Material> paint{ RES(Material, "Materials/Paint.xml")->Clone() };

        Color spec{};
        spec.FromHSV(.0f, .0f, .9f, 42.f);

        paint->SetShaderParameter("MatDiffColor", shade.ToColor());
        paint->SetShaderParameter("MatSpecColor", spec);
        paint_.Insert({ shade, paint });
    }

    return paint_[shade];
}

Material* Materials::Skin(const Tone& tone)
{
    if (!skin_.Contains(tone))
    {
        SharedPtr<Material> skin{ RES(Material, "Materials/Skin.xml")->Clone() };

        skin->SetShaderParameter("MatDiffColor", tone.Diffuse());
        skin->SetShaderParameter("MatSpecColor", tone.Specular());
        skin_.Insert({ tone, skin });
    }

    return skin_[tone];
}

Material* Materials::Hair(const Tint& tint)
{
    if (!hair_.Contains(tint))
    {
        SharedPtr<Material> hair{ RES(Material, "Materials/Hair.xml")->Clone() };

        hair->SetShaderParameter("MatDiffColor", tint.Diffuse());
        hair->SetShaderParameter("MatSpecColor", tint.Specular());
        hair_.Insert({ tint, hair });
    }

    return hair_[tint];
}

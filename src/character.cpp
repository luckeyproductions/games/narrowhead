/* Narrowhead
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "materials.h"

#include "character.h"

Character::Character(Context* context): Controllable(context),
    blue_{ Random(2) },
    male_{ Random(2) }
{
}

void Character::OnNodeSet(Node* node)
{
    if (!node)
        return;

    Controllable::OnNodeSet(node);
    Materials* materials{ GetSubsystem<Materials>() };
    SharedPtr<Material> cloth{ RES(Material, (blue_ ? "Materials/Uniform.xml" : "Materials/Jumpsuit.xml")) };

    model_->SetModel(RES(Model, (male_ ? "Models/Male.mdl" : "Models/Female.mdl")));
    model_->SetCastShadows(true);
    const int skinRoll{ Min(7, DiceRoll(3, 4) - 3) };
    model_->SetMaterial(0, materials->Skin({ (blue_ ? skinRoll : 7 - skinRoll) }));
    model_->SetMaterial(1, cloth);
    model_->SetMaterial(2, cloth);
    model_->SetMaterial(3, materials->Cloth());
    model_->SetMaterial(4, materials->Hair());


    for (int m{ 0 }; m < model_->GetNumMorphs(); ++m)
        model_->SetMorphWeight(m, DiceRoll(3, 3) * .25f - .5f);

    const float scale{ 2/3.f };

    node_->SetScale(Vector3{ scale, 1.f, scale } * scale);

    animCtrl_->PlayExclusive("Models/IdleAlert.ani", 0, true);
    animCtrl_->SetTime("Models/IdleAlert.ani", Random(animCtrl_->GetLength("Models/IdleAlert.ani")));
}

void Character::Update(float timeStep)
{
    Controllable::Update(timeStep);

    node_->Translate(move_ * timeStep, TS_WORLD);

    if (move_.LengthSquared() > 0.f)
        AlignWithMovement(timeStep * .55f);
}

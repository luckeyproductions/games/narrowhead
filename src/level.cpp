/* Narrowhead
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "character.h"

#include "level.h"

Level::Level(Context* context) : Component(context),
    musicSource_{ nullptr },
    floors_{},
    walls_{},
    boxes_{ nullptr }
{
}

void Level::OnSetEnabled() { Component::OnSetEnabled(); }

void Level::OnNodeSet(Node* node)
{
    musicSource_ = GetScene()->CreateComponent<SoundSource>();
//    PlayMusic();

    boxes_ = node->CreateComponent<StaticModelGroup>();
    boxes_->SetModel(CACHE->GetResource<Model>("Models/Box.mdl"));
    boxes_->SetMaterial(RES(Material, "Materials/Concrete.xml"));

    HashSet<IntVector2> lights{};
    HashSet<IntVector2> streets{};
    HashSet<IntVector2> walls{};

    for (int x{ -9 }; x < 3; ++x)
    for (int y{  -5 }; y < 4; ++y)
    {
        if (x % 6 == 0 || y % 3 == 0)
            streets.Insert(IntVector2{ x, y });
    }


    for (const IntVector2& street: streets)
    {
        int ways{ 0 };

        for (int x{ -1 }; x <= 1; ++x)
        for (int y{ -1 }; y <= 1; ++y)
        {
            if (x == 0 && y == 0)
                continue;

            const IntVector2 coord{ street + IntVector2{ x, y } };

            if (!streets.Contains(coord))
                walls.Insert(coord);
            else if (Abs(x) != Abs(y))
                ways++;
        }

        Node* streetNode{ node_->CreateChild("Street") };
        const Vector3 streetPosition{ 1.f * street.x_, -1.f, 1.f * street.y_ };
        streetNode->SetPosition(streetPosition);
        boxes_->AddInstanceNode(streetNode);
        floors_.Insert({ street, streetNode });

        if (ways != 2 || Abs(street.x_ % 6) == 3)
            lights.Insert(street);

        // Characters
        if (Random(3) == 0)
        {
            Node* characterNode{ GetScene()->CreateChild("Character") };
            characterNode->CreateComponent<Character>();
            characterNode->SetPosition(streetPosition + Vector3{ RandomOffCenter(.4f), .5f, RandomOffCenter(.4f) });

            characterNode->SetDirection((-characterNode->GetPosition() + VectorRound(characterNode->GetPosition())).ProjectOntoPlane(Vector3::UP).NormalizedOrDefault(Vector3::FORWARD));

        }
    }

    for (const IntVector2& wall: walls)
    {
        Node* wallNode{ node_->CreateChild("Building") };
        wallNode->SetPosition(Vector3{ wall.x_, 0.f, wall.y_ });
        wallNode->SetScale({ 1.f, M_PHI, 1.f });
        boxes_->AddInstanceNode(wallNode);

        walls_.Insert({ wall, wallNode });
    }

    const Color lightColor{ Color::WHITE.Lerp(Color::ORANGE.Lerp(Color::YELLOW, .2f), .2f) };

    GetSubsystem<Renderer>()->GetDefaultZone()->SetAmbientColor(lightColor.Lerp(Color::YELLOW, -.75f).Lerp(Color::BLACK, .875f));

    for (const IntVector2& light: lights)
    {
        Node* floorNode{ floors_[light] };
        Node* lightNode{ floorNode->CreateChild("Light") };
        Node* shadeNode{ lightNode->CreateChild("Shade") };
        shadeNode->Pitch(-90.f);
        shadeNode->SetScale({ .2f, .08f, .2f });
        shadeNode->SetPosition(Vector3::FORWARD * .8f);
        StaticModel* shade{ shadeNode->CreateComponent<StaticModel>() };
        shade->SetModel(RES(Model, "Models/Cone.mdl"));
        shade->SetLightMask(1 << 23);

        lightNode->SetDirection(Vector3::DOWN);
        lightNode->SetPosition(Vector3::UP * 2.3f);

        Light* spot{ lightNode->CreateComponent<Light>() };
        Node* pointNode{ lightNode->CreateChild("Point") };
        pointNode->SetPosition(Vector3::FORWARD * 1.5f);
        Light* point{ pointNode->CreateComponent<Light>() };


        spot->SetLightType(LIGHT_SPOT);
        spot->SetCastShadows(true);
        spot->SetRange(2.3f);
        spot->SetBrightness(1.5f);
        spot->SetFov(62.5f);
        spot->SetLightMask(M_MAX_UNSIGNED - (1 << 23));
        spot->SetColor(lightColor);

        point->SetRange(2.5f);
        point->SetBrightness(.5f);
        point->SetSpecularIntensity(.0f);
        point->SetColor(lightColor.Lerp(Color::YELLOW, -.05f));
    }

    UpdateVisibility();
}

void Level::PlayMusic()
{
    SharedPtr<Sound> music{ RES(Sound, "Music/Kevin Hartnell - Cells.ogg") };

    music->SetLooped(true);
    musicSource_->SetSoundType(SOUND_MUSIC);
    musicSource_->Play(music);
}

void Level::UpdateVisibility()
{
    const IntVector2 position{ 0, 0 };
    const IntVector2 direction{ 0, 1 };

    if (direction == IntVector2::ZERO)
        return;

    for (IntVector2& wall: walls_.Keys())
    {
//        if (direction.x_ == 0)
        walls_[wall]->SetEnabled(Sign(wall.y_ - position.y_ + direction.y_) == Sign(direction.y_));
//        else
//            walls_[wall]->SetEnabled(wall.x_ - position.x_ >= 0);
    }

    for (IntVector2& floor: floors_.Keys())
    {
//        if (direction.x_ == 0)
            floors_[floor]->SetEnabled(Sign(floor.y_ - position.y_ + direction.y_ * 3) == Sign(direction.y_));
//        else
//            floors_[floor]->SetEnabled(floor.x_ - position.x_ >= 0);
    }
}

void Level::OnSceneSet(Scene* scene) {}
void Level::OnMarkedDirty(Node* node) {}
void Level::OnNodeSetEnabled(Node* node) {}
bool Level::Save(Serializer& dest) const { return Component::Save(dest); }
bool Level::SaveXML(XMLElement& dest) const { return Component::SaveXML(dest); }
bool Level::SaveJSON(JSONValue& dest) const { return Component::SaveJSON(dest); }
void Level::MarkNetworkUpdate() { Component::MarkNetworkUpdate(); }
void Level::GetDependencyNodes(PODVector<Node*>& dest) {}
void Level::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}

/* Narrowhead
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "jib.h"

Jib::Jib(Context* context): Component(context),
    target_{ nullptr }
{
}

void Jib::OnSetEnabled() { Component::OnSetEnabled(); }
void Jib::OnNodeSet(Node* node)
{
    if (!node)
        return;

    Camera* camera{ node_->CreateComponent<Camera>() };
    Viewport* viewport{ new Viewport{ context_, GetScene(), camera } };
    RenderPath* effectRenderPath{ viewport->GetRenderPath() };
    effectRenderPath->Load(RES(XMLFile, "RenderPaths/Forward.xml"));
    RENDERER->SetViewport(0, viewport);
    effectRenderPath->Append(RES(XMLFile, "PostProcess/FXAA3.xml"));
//    effectRenderPath->SetEnabled("FXAA3", true);

    effectRenderPath->Append(RES(XMLFile, "PostProcess/BloomHDR.xml"));
    effectRenderPath->SetShaderParameter("BloomHDRThreshold", .25f);
    effectRenderPath->SetShaderParameter("BloomHDRMix", Vector2{ 0.95f, .25f });
//    effectRenderPath->SetEnabled("BloomHDR", true);

    camera->SetFov(60.f);

    SubscribeToEvent(E_POSTUPDATE, DRY_HANDLER(Jib, Update));
}

void Jib::SetTarget(Node* target)
{
    target_ = target;

    Vector3 targetPosition{ target_->GetWorldPosition() };

    node_->SetPosition({ targetPosition.x_, .25f, RoundToInt(targetPosition.y_) - 2.3f });
    node_->LookAt({ targetPosition.x_, .125f, RoundToInt(targetPosition.y_) });
}

void Jib::Update(StringHash eventType, VariantMap& eventData)
{
    Vector3 targetPosition{ target_->GetWorldPosition() };

    node_->SetPosition({ targetPosition.x_, .25f, RoundToInt(targetPosition.y_) - 2.3f });
    node_->LookAt({ targetPosition.x_, .125f, RoundToInt(targetPosition.y_) });
}

void Jib::OnSceneSet(Scene* scene) { Component::OnSceneSet(scene); }
void Jib::OnMarkedDirty(Node* node) {}
void Jib::OnNodeSetEnabled(Node* node) {}
bool Jib::Save(Serializer& dest) const { return Component::Save(dest); }
bool Jib::SaveXML(XMLElement& dest) const { return Component::SaveXML(dest); }
bool Jib::SaveJSON(JSONValue& dest) const { return Component::SaveJSON(dest); }
void Jib::MarkNetworkUpdate() { Component::MarkNetworkUpdate(); }
void Jib::GetDependencyNodes(PODVector<Node*>& dest) {}
void Jib::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}

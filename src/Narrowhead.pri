HEADERS += \
    $$PWD/character.h \
    $$PWD/controllable.h \
    $$PWD/inputmaster.h \
    $$PWD/jib.h \
    $$PWD/level.h \
    $$PWD/luckey.h \
    $$PWD/mastercontrol.h \
    $$PWD/materials.h \
    $$PWD/player.h \
    $$PWD/sceneobject.h \
    $$PWD/spawnmaster.h \

SOURCES += \
    $$PWD/character.cpp \
    $$PWD/controllable.cpp \
    $$PWD/inputmaster.cpp \
    $$PWD/jib.cpp \
    $$PWD/level.cpp \
    $$PWD/luckey.cpp \
    $$PWD/mastercontrol.cpp \
    $$PWD/materials.cpp \
    $$PWD/player.cpp \
    $$PWD/sceneobject.cpp \
    $$PWD/spawnmaster.cpp \

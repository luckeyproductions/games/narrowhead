/* Narrowhead
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "level.h"
#include "inputmaster.h"
#include "player.h"
#include "spawnmaster.h"
#include "materials.h"
#include "jib.h"

#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl* MasterControl::instance_ = NULL;

MasterControl* MasterControl::GetInstance()
{
    return MasterControl::instance_;
}

MasterControl::MasterControl(Context *context):
    Application(context)
{
    instance_ = this;
}

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs") + "Narrowhead.log";
    engineParameters_[EP_WINDOW_TITLE] = "Narrowhead";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_WORKER_THREADS] = false;
    engineParameters_[EP_RESOURCE_PATHS] = "Resources;";
}
void MasterControl::Start()
{
    RegisterSubsystem<InputMaster>();
    RegisterSubsystem<SpawnMaster>();
    RegisterSubsystem<Materials>();

    RegisterObject<Level>();
    RegisterObject<Character>();
    context_->RegisterFactory<Jib>();

    if (GRAPHICS)
        ENGINE->SetMaxFps(GRAPHICS->GetRefreshRate());

    CreateScene();
}

void MasterControl::Stop()
{
    engine_->DumpResources(true);
}

void MasterControl::Exit()
{
    engine_->Exit();
}

void MasterControl::CreateScene()
{
    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();
    scene_->CreateComponent<PhysicsWorld>();
    scene_->CreateComponent<DebugRenderer>();


    //Camera
    Node* cameraNode{ scene_->CreateChild("Camera") };
    Jib* jib{ cameraNode->CreateComponent<Jib>() };

    scene_->CreateChild()->CreateComponent<Level>();

    const Vector2 startCoords{ -9.25f + Random(12), RandomOffCenter(.25f) };
    Node* characterNode{ scene_->CreateChild("Character") };
    characterNode->SetPosition({ startCoords.x_, -0.5f, startCoords.y_ });
    characterNode->SetDirection(-VectorMod({ startCoords.x_, 0.f, startCoords.y_ }, Vector3::ONE).Normalized());
    GetSubsystem<InputMaster>()->SetPlayerControl(AddPlayer(), characterNode->CreateComponent<Character>());

    jib->SetTarget(characterNode);
}

Vector<SharedPtr<Player> > MasterControl::GetPlayers()
{
    return players_;
}

Player* MasterControl::AddPlayer()
{
    players_.Push(MakeShared<Player>(players_.Size() + 1, context_));

    return players_.Back();
}

Player* MasterControl::GetPlayer(int playerId) const
{
    for (Player* p : players_) {

        if (p->GetPlayerId() == playerId){
            return p;
        }
    }
    return nullptr;
}

Player* MasterControl::GetNearestPlayer(Vector3 pos)
{
    Player* nearest{};
    for (Player* p : players_){
        if (p->IsAlive()){

            if (!nearest
                    || (LucKey::Distance(GetSubsystem<InputMaster>()->GetControllableByPlayer(p->GetPlayerId())->GetWorldPosition(), pos) <
                        LucKey::Distance(GetSubsystem<InputMaster>()->GetControllableByPlayer(nearest->GetPlayerId())->GetWorldPosition(), pos)))
            {
                nearest = p;
            }
        }
    }
    return nearest;
}

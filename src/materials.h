/* Narrowhead
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MATERIALS_H
#define MATERIALS_H

#include "mastercontrol.h"

struct Shade: public IntVector3 // Cloth and paint
{
    Shade(int hue, int sat, int val): IntVector3(hue, sat, val) {}
    Shade(): Shade(0, 1, 1) {}

    int Hue() const { return x_; }
    int Sat() const { return y_; }
    int Val() const { return z_; }

    void SetHue(int hue) { x_ = hue; }
    void SetSat(int sat) { y_ = sat; }
    void SetVal(int val) { z_ = val; }

    Color ToColor() const // 64 hues * 8 saturations * 8 values
    {
        Color c; c.FromHSV( x_ / 64.f,
                            y_ / 8.f,
                            z_ / 8.f ); return c;
    }

    static Shade Pick()
    {
        Shade shade{ Random(64), Random(8), Random(8) };
        shade.Validate();

        return shade;
    }

    void Validate()
    {
        // Black is black
        if (Val() == 0)
        {
            SetHue(0); SetSat(0);
        }
        // Grey is grey
        if (Sat() == 0)
        {
            SetHue(0);
        }
    }
};

struct Tone: public IntVector2 // Skin
{
    Tone(int bleak, int red): IntVector2(bleak, red) {}
    Tone(int bleak): IntVector2(bleak, DiceRoll(2, 3) - 4) {}
    Tone(): Tone(4, 0) {}

    int Bleak() const { return x_; }
    int Red() const { return y_; }

    Tone Lips() const { return { (Bleak() + 8) / 3, Red() + 1 }; }
    Shade Eyes() const
    {
        int hue{ 4 * Clamp(DiceRoll(Bleak(), Bleak()) - Bleak() * 2, 0, 9) };

        return { hue,
                 4 + Bleak() / 3,
                 2 + Bleak() / 3 };
    }

    Color Diffuse() const
    {
        Color c;
        c.FromHSV( Clamp(.025f - Red() * Red() * Sign(Red()) * .005f, 0.f, 1.f),
                   .6f - (Bleak() * .5f + Red()) * .055f,
                   (2 + Bleak() * .9f + Red() / 2) * .1f );
        return c;
    }

    Color Specular() const
    {
        Color c{ Diffuse() };
        c = c.Lerp(Color::GRAY.Lerp(Color::BLACK, Bleak() * .1f), .9f);
        c.a_ = 25.f - 2.5f * Bleak();

        return c;

    }

    static Tone Pick()
    {
        int bleak{ Min(7, DiceRoll(3, 4) - 3) };
        int red{ DiceRoll(2, 3) - 4 };

        if (bleak == 0 || bleak == 7)
            red = Clamp(red, -1, 1);

        return { bleak, red };
    }

};

struct Tint: public IntVector2 // Hair
{
    Tint(int blond, int red): IntVector2(blond, red) {}
    Tint(): Tint(4, 0) {}

    int Blond() const { return x_; }
    int Red() const { return y_; }

    Color Diffuse() const
    {
        Color c;
        const int b{ Blond() };
        const int r{ Red() };

        c.FromHSV( .08f + (.2f * b - r * Abs(r) * .75f) * .02f,
                   .7f + r * .075f - Abs(b - 2) * .15f,
                   Min(1.f, .05f + (b - Abs(r) * .3f) * .2f) );
        return c;
    }

    Color Specular() const
    {
        Color c{ Diffuse() };
        c = c.Lerp(Color::GRAY.Lerp(Color::RED, Max(0, Red()) * .05f), .8f);
        c.a_ = 1.25f + 1.25f * Blond();

        return c;
    }

    static Tint Pick(Tone skinTone)
    {
        Tint tint{ DiceRoll(1 + skinTone.Bleak() / 4, Max(2, skinTone.Bleak() / 2 - 1)) - 1,
                   Clamp(DiceRoll(2, 3) - 4,
                         2 - Abs(skinTone.Red()),
                         skinTone.Bleak() - Abs(skinTone.Red())) };

        return tint;
    }
};

class Materials: public Object
{
    DRY_OBJECT(Materials, Object);

public:
    Materials(Context* context): Object(context),
        cloth_{},
        paint_{},
        skin_{},
        hair_{}
    {}

    Material* Cloth(const Shade& shade);
    Material* Cloth()
    {
        Shade shade{ Random(0100), DiceRoll(3, 3) - 3, DiceRoll(3, 3) - 2 };

        return Cloth(shade);
    }

    Material* Paint(const Shade& shade);
    Material* Paint()
    {
        Shade shade{ Shade::Pick() };

        return Paint(shade);
    }

    Material* Skin(const Tone& tone);
    Material* Skin()
    {
        return Skin(Tone::Pick());
    }

    Material* Hair(const Tint& tint);
    Material* Hair(const Tone& tone)
    {
        return Hair(Tint::Pick(tone));
    }

    Material* Hair()
    {
        return Hair(Tone::Pick());
    }

private:
    HashMap<Shade, SharedPtr<Material> > cloth_;
    HashMap<Shade, SharedPtr<Material> > paint_;
    HashMap<Tone, SharedPtr<Material> > skin_;
    HashMap<Tint, SharedPtr<Material> > hair_;
};

#endif // MATERIALS_H
